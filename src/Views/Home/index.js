import React, { useGlobal } from "reactn";
import { Card, Col, Container, Image, Row } from "react-bootstrap";

import { namedRoutes } from "Routes";
import { useTitle } from "Utils";
import {
  Alert,
  Calendar,
  Groceries,
  MapAndLocation,
  Settings,
  Todo,
} from "Components/Icons";
import "Assets/scss/pages/home.scss";
export default ({ history }) => {
  const user = useGlobal("user");
  useTitle("DoTheDo");

  const cards = [
    {
      title: "To Do",
      subTitle: "active",
      caption: "4 items",
      icon: Todo,
      link: namedRoutes.todo.index,
    },
    {
      title: "Calendar",
      subTitle: "Today's date",
      caption: "2 events",
      icon: Calendar,
      link: namedRoutes.calendar.index,
    },
    {
      title: "Activity",
      subTitle: "You completed...",
      icon: Alert,
    },
    {
      title: "Location",
      subTitle: "Left the office",
      icon: MapAndLocation,
    },
    {
      title: "Groceries",
      subTitle: "Tomatoes...",
      caption: "4 items",
      icon: Groceries,
    },
    {
      title: "Settings",
      subTitle: "Today's date",
      caption: "2 events",
      icon: Settings,
    },
  ];

  return (
    <Container className="py-5">
      <div className="greeting mb-5">
        <span className="greeting__head">Hello</span>
        <br />
        <b className="greeting__name mb-2">{user},</b>
        <span className="greetingHead">
          Hello! what would you like to do today?
        </span>
      </div>
      <Row className="row cards">
        {cards.map(({ title, subtitle, caption, icon, link }, key) => (
          <Col xs="6" md="4" lg="3" className="py-3" key={key}>
            <Card
              className="shadow-sm card__item h-100 rounded"
              onClick={() => link && history.push(link)}
            >
              <div className="text-center p-3">
                <Image src={icon} className="m-2" fluid />

                {title && (
                  <h6 className="m-0 text-dark">
                    <b>{title}</b>
                  </h6>
                )}
                {subtitle && <small className="text-muted">{subtitle}</small>}
                {caption && (
                  <div>
                    <small>{caption}</small>
                  </div>
                )}
              </div>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};
