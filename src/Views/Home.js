import React from "react";
import { Switch, Route } from "react-router-dom";

import { namedRoutes } from "Routes";
import Home from "./Home/";

import "Assets/scss/pages/home.scss";

export default () => (
  <Switch>
    <Route exact path={namedRoutes.home.index} component={Home} />
  </Switch>
);
