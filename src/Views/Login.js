import React from "react";
import { Switch, Route } from "react-router-dom";
import Login from "./auths/Login";
import { namedRoutes } from "Routes";

export default () => (
  <Switch>
    <Route exact path={namedRoutes.login.index} component={Login} />
  </Switch>
);
