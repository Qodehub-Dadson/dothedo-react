import React, { useState, Fragment } from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import useSWR from "swr";

import { Modal, Spinner } from "Components";
import { namedRoutes } from "Routes";
import ListTodos from "./Components/TodoList";
import Update from "./Forms/Update";
import New from "./Forms/New";

export default () => {
  const { data: todos, mutate, error } = useSWR("/todos");
  const [todo, setTodo] = useState(null);
  const [modal, setModal] = useState(false);
  const completed = [];
  const notComplete = [];

  const listTodo = todos || [];

  listTodo.forEach((element) => {
    if (element.completed === true) {
      completed.push(element);
    } else {
      notComplete.push(element);
    }
  });

  const detailCards = [
    {
      count_name: "All",
      count_icon: "format_list_numbered",
      count_number: todos?.length,
      count_color: "secondary",
    },
    {
      count_name: "Active",
      count_icon: "local_offer",
      count_number: notComplete?.length,
      count_color: "success",
    },
    {
      count_name: "Completed",
      count_icon: "fact_check",
      count_number: completed?.length,
      count_color: "danger",
    },
  ];

  return (
    <Fragment>
      <Container className="pt-3 p-0">
        <Link
          to={namedRoutes.home.index}
          className="d-flex align-items-center text-muted mb-4 px-3 text-decoration-none"
        >
          <i className="fa fa-arrow-left d-block mr-2" /> Back
        </Link>
        <div className="my-2 mb-4 p-0">
          <div className="scrolling-wrapper text-center px-2">
            {detailCards.map(
              ({ count_name, count_icon, count_color, count_number }, key) => (
                <div className="bean" key={key}>
                  <div className="card1">
                    <div className={`card-counter ${count_color}`}>
                      <i className="card-icon material-icons">{count_icon}</i>
                      <span className="count-numbers text-right">
                        {count_number || 0}
                      </span>
                      <small className="count-name">{count_name}</small>
                    </div>
                  </div>
                </div>
              )
            )}
          </div>
        </div>
      </Container>
      <Container>
        <div className="mb-4">
          <New {...{ mutate }} />
        </div>
        {!todos && !error ? (
          <Spinner contained />
        ) : (
          <ListTodos {...{ mutate, todos, setTodo, setModal }} />
        )}
      </Container>

      <Modal header="Update Todo" onHide={() => setModal(false)} show={modal}>
        <Update {...{ mutate, setModal, todo }} />
      </Modal>
    </Fragment>
  );
};
