import React from "react";
import { Formik } from "formik";

import { createTodoService } from "Services/todoService";
import Form, { Schema } from "./Form";

export default ({ mutate }) => (
  <Formik
    validateOnMount
    initialValues={{ title: "", completed: false }}
    validationSchema={Schema}
    children={(props) => <Form {...props} />}
    onSubmit={(params, { setSubmitting, resetForm }) =>
      createTodoService(params)
        .then(() => mutate() | resetForm())
        .catch(() => {})
        .finally(() => setSubmitting())
    }
  />
);
