import React from "react";
import { Formik } from "formik";

import { updateTodoService } from "Services/todoService";
import Form, { Schema } from "./Form";

export default ({ todo, setModal, mutate }) => (
  <Formik
    validateOnMount
    initialValues={{ title: todo?.title, completed: todo?.completed }}
    validationSchema={Schema}
    children={(props) => <Form {...props} />}
    onSubmit={(params, { setSubmitting }) =>
      updateTodoService({ ...todo, ...params })
        .then(() => mutate() | setModal(false))
        .catch(() => {})
        .finally(() => setSubmitting(false))
    }
  />
);
