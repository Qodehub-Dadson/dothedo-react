import React from "react";
import { Form } from "react-bootstrap";
import { object, string } from "yup";

import { Button, Field } from "Components";

export const Schema = object({ title: string().required("Title is requried") });

export default ({ handleSubmit, isValid, isSubmitting, values }) => {
  const { title } = values;

  return (
    <Form>
      <Form.Group className="d-flex w-100">
        <Field
          name="title"
          value={title}
          placeholder="Enter todo name here"
          containerProps={{ className: "mb-0 w-100" }}
        />
        <Button
          className="px-4 btn-48 default-border default-text"
          {...{ isSubmitting, isValid }}
          onClick={handleSubmit}
          loadingColor="#000"
          value="Submit"
          type="submit"
        />
      </Form.Group>
    </Form>
  );
};
