import React, { Fragment } from "react";
import { Card } from "react-bootstrap";
import Moment from "react-moment";
import { deleteTodoService, updateTodoService } from "Services/todoService";

export default ({ todos, setTodo, setModal, mutate }) => {
  const handleDelete = async (id) => {
    if (await window.confirm("Are you sure you want to delete this item")) {
      deleteTodoService(id).then(() => mutate());
    }
  };

  const handleComplete = async (el) => {
    el.completed = !el.completed;
    if (await window.confirm("Are you sure you have completed this item")) {
      updateTodoService(el).then(() => mutate());
    }
  };

  return (
    <Fragment>
      {todos?.map((el, key) => (
        <Card className="shadow-sm mb-4" key={key}>
          <Card.Body className="d-flex justify-content-between align-items-center">
            <div>
              <h5 className="mb-0">{el.title}</h5>
              <small className="text-muted">
                <Moment fromNow ago>
                  {el.created_at}
                </Moment>
                <span className="ml-1">ago</span>
              </small>
            </div>
            <div className="d-flex align-items-center mx-n2">
              <span onClick={() => handleComplete(el)} className="px-2">
                <i
                  className={`material-icons ${
                    el.completed ? "text-warning" : "text-success"
                  }`}
                >
                  {el.completed ? "close" : "check"}
                </i>
              </span>
              <span
                onClick={() => setTodo(el) | setModal(true)}
                className="px-2"
              >
                <i className="material-icons-outlined text-info">edit</i>
              </span>
              <span onClick={() => handleDelete(el.id)} className="px-2">
                <i className="material-icons-outlined text-danger">delete</i>
              </span>
            </div>
          </Card.Body>
        </Card>
      ))}
    </Fragment>
  );
};
