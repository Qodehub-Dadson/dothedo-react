import React from "react";
import { Formik } from "formik";

import Form, { Schema } from "./Form";

export default () => (
  <Formik
    validateOnMount
    initialValues={{ date: "", completed: false }}
    validationSchema={Schema}
    children={(props) => <Form {...props} />}
  />
);
