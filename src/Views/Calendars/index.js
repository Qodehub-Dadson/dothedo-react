import React, { Fragment } from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

// import { Modal, Spinner } from "Components";
import { namedRoutes } from "Routes";
import New from "./Forms/NewDate";

export default () => {
  // const [modal, setModal] = useState(false);

  return (
    <Fragment>
      <Container className="pt-3">
        <Link
          to={namedRoutes.home.index}
          className="d-flex align-items-center text-muted mb-4 text-decoration-none"
        >
          <i className="fa fa-arrow-left d-block mr-2" /> Back
        </Link>
        <div className="my-4">
          <h3>Calendar</h3>
          <New />
        </div>
      </Container>
    </Fragment>
  );
};
