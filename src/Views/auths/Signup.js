import React, { Fragment } from "react";
import { Container } from "react-bootstrap";

import Forms from "./Forms/SignupForm";

export default () => {
  return (
    <Fragment>
      <Container className="pt-3">
        <div className="my-4 text-center">
          <h3>Signup</h3>
        </div>
        <Forms />
      </Container>
    </Fragment>
  );
};
