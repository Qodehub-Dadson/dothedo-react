import React from "react";
import { Form } from "react-bootstrap";
import { object, string } from "yup";
import { Link } from "react-router-dom";
import { Button, Field } from "Components";
import { namedRoutes } from "Routes";
export const Schema = object({
  password: string().required("Please enter password"),
  email: string().required("Email is requried"),
});

export default ({ handleSubmit, isValid, isSubmitting, values }) => {
  const { email, password } = values;

  return (
    <Form>
      <Form.Group className="w-100">
        <Field
          name="email"
          type="email"
          value={email}
          placeholder="Enter email"
          containerProps={{ className: "mb-3 w-100" }}
        />
        <Field
          name="password"
          type="password"
          value={password}
          placeholder="Enter password"
          containerProps={{ className: "mb-3 w-100" }}
        />
        <div>
          Don't have an account?
          <Link to={namedRoutes.signup.index}> Signup</Link>
        </div>
        <div className="text-right mt-3">
          <Button
            className="px-4 btn btn-outline-info"
            {...{ isSubmitting, isValid }}
            onClick={handleSubmit}
            loadingColor="#fff"
            value="Login"
            type="submit"
          />
        </div>
      </Form.Group>
    </Form>
  );
};
