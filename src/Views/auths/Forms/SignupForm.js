import React from "react";
import { Formik } from "formik";

import Form, { Schema } from "./Formsignup";

export default () => (
  <Formik
    validateOnMount
    initialValues={{ email: "", password: "" }}
    validationSchema={Schema}
    children={(props) => <Form {...props} />}
  />
);
