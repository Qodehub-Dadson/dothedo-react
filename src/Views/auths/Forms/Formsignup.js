import React from "react";
import { Form } from "react-bootstrap";
import { object, string } from "yup";
import { Link } from "react-router-dom";
import { Button, Field } from "Components";
import { namedRoutes } from "Routes";
export const Schema = object({
  password: string().required("Password is required"),
  email: string().required("Email is requried"),
  username: string().required("Username is requried"),
});

export default ({ handleSubmit, isValid, isSubmitting, values }) => {
  const { email, password, username } = values;

  return (
    <Form>
      <Form.Group className="w-100">
        <Field
          name="username"
          type="text"
          value={username}
          placeholder="Username"
          containerProps={{ className: "mb-3 w-100" }}
        />
        <Field
          name="email"
          type="email"
          value={email}
          placeholder="Email"
          containerProps={{ className: "mb-3 w-100" }}
        />
        <Field
          name="password"
          type="password"
          value={password}
          placeholder="Password"
          containerProps={{ className: "mb-3 w-100" }}
        />
        <div>
          Already have an account?
          <Link to={namedRoutes.login.index}> Login</Link>
        </div>
        <div className="text-right mt-3">
          <Button
            className="px-4 btn btn-outline-info"
            {...{ isSubmitting, isValid }}
            onClick={handleSubmit}
            loadingColor="#fff"
            value="Login"
            type="submit"
          />
        </div>
      </Form.Group>
    </Form>
  );
};
