import React from "react";
import { Formik } from "formik";

import { loginService } from "Services/authService";
import Form, { Schema } from "./FormLogin";

export default () => (
  <Formik
    validateOnMount
    initialValues={{ email: "", password: "" }}
    validationSchema={Schema}
    children={(props) => <Form {...props} />}
    onSubmit={(params, { setSubmitting, resetForm }) =>
      loginService(params)
        .then(() => {})
        .catch(() => {})
        .finally(() => setSubmitting())
    }
  />
);
