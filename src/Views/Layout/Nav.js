import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import "Assets/scss/pages/layout.scss";

export default () => (
  <Navbar expand="lg" className="navbar" bg="light" variant="light">
    <Container>
      <Navbar.Brand href="/" className="navbar__brand">
        <div className="navbar__head"> DoTheDo </div>
      </Navbar.Brand>
      <Navbar.Toggle
        className="navbar__toggle"
        aria-controls="responsive-navbar-nav"
      />
      <Navbar.Collapse className="navbar__collapse" id="responsive-navbar-nav">
        <Nav className="ml-auto navbar__navlinks">
          <Nav.Link href="/">Home</Nav.Link>
          <Nav.Link href="/">Profile</Nav.Link>
          <Nav.Link href="/">Support</Nav.Link>
          <Nav.Link href="/">Logout</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
);
