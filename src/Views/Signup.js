import React from "react";
import { Switch, Route } from "react-router-dom";
import Signup from "./auths/Signup";
import { namedRoutes } from "Routes";

export default () => (
  <Switch>
    <Route exact path={namedRoutes.signup.index} component={Signup} />
  </Switch>
);
