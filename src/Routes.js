import React, { Fragment } from "react";
/**
 * packages
 */
import {
  BrowserRouter as Router,
  Switch,
  Route,
  // useLocation,
} from "react-router-dom";

import { ScrollToTop } from "Components";

/**
 * Route to base components
 */
import Home from "Views/Home";
import Todo from "Views/Todo";
import Calendar from "Views/Calendar";
import Login from "Views/Login";
import Signup from "Views/Signup";
import Nav from "Views/Layout/Nav";
import ProtectRoute from "ProtectedRoutes";
// export const { location } = useLocation();

export default () => (
  <Fragment>
    {/* {location.pathname.match("/login") || location.pathname.match("/signup") ? ( */}
    <Nav />
    {/* // ) : (
    //   <span></span>
    // )} */}
    <Router>
      <ScrollToTop />
      <Switch>
        <ProtectRoute exact path="/calendar" component={Calendar} />
        <ProtectRoute exact path="/todos" component={Todo} />
        <ProtectRoute exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Signup} />
      </Switch>
    </Router>
  </Fragment>
);

/**
 * namedRoutes
 *
 * Sometimes it is very import to name your routes if there's
 * a possibility of the routes changing often.
 *
 * This ensures that you have your routes defined at a central
 * place where you can update and have it reflect everywhere it
 * is used
 *
 * Usage
 *
 * import {namedRoutes} from "Routes";
 *
 * namedRoutes.home.index
 * namedRoutes.settings.profile
 * ....
 */
export const namedRoutes = {
  home: { index: "/" },
  todo: { index: "/todos" },
  calendar: { index: "/calendar" },
  signup: { index: "/signup" },
  login: { index: "/login" },
};
