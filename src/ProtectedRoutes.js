import React from "react";
import { useGlobal } from "reactn";
import { Route, Redirect } from "react-router-dom";

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
export default ({ component: Comp, ...rest }) => {
  const isAuthenticated = useGlobal("isAuthenticated");

  return (
    <Route
      {...rest}
      component={(props) =>
        isAuthenticated ? (
          <Comp {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
