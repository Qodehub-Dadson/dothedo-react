import { Http } from "Utils";

export const createTodoService = (payload) => Http.post("/todo", payload);
export const updateTodoService = (payload) =>
  Http.patch(`/todo/${payload.id}`, payload);
export const deleteTodoService = (id) => Http.delete(`/todo/${id}`);
