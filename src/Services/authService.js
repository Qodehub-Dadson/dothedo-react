import { Http } from "Utils";

export const loginService = (payload) => Http.post("/login", payload);
export const signupService = (payload) => Http.post("/signup", payload);
