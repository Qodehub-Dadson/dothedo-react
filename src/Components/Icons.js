export { default as Todo } from "Assets/images/list.svg";
export { default as Alert } from "Assets/images/alert.svg";
export { default as Settings } from "Assets/images/settings.svg";
export { default as Calendar } from "Assets/images/google-calendar.svg";
export { default as Groceries } from "Assets/images/groceries.svg";
export { default as MapAndLocation } from "Assets/images/maps-and-location.svg";
